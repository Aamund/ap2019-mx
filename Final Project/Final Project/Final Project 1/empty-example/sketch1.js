//audio capture
var mic;

//audio output
var tansaVoice = new p5.Speech('en-US');
tansaVoice.setLang('en-UK');
tansaVoice.setVoice('Google UK English Male');

//images
var imgMic;
var imgLogo;

//loading throbber
var k = 99,r = 70;

//text font
var ralewayFont;

//JSON
var tansasBrain
var replies = [];

function preload(){
  tansasBrain = loadJSON('data/TANSASbrain.json')
  imgMic = loadImage('data/micicon.png');
  imgLogo = loadImage('data/logo.png');
  ralewayFont = loadFont('data/Raleway-Light.ttf')
}

function setup(){
reset();
}

// function keyReleased() {
// if (keyCode === 32) {
// loading();
// var sentences = tansasBrain.answers;
// var rdmanswer = random(sentences);
// fill(2,23,142);
// textFont(ralewayFont);
// textSize(20)
// textAlign(CENTER)
// text(rdmanswer, windowWidth/2, windowHeight/2)
// text(random(tansasBrain.answers), windowWidth/2, windowHeight/2);

// function createReplies() {
// for (var i = 0; i < tansasBrain.answers.length; i++) {
//   replies.push(tansasBrain.answers);
// }
// }

//tansaVoice.speak(random(tansasBrain.answers))
// }
// }
// return false;
// }

function draw() {
var vol = mic.getLevel();

background(255);
fill(2,23,142);
noStroke()
ellipse(windowWidth/2, windowHeight/4*3,100);

//interaction
if(keyIsDown(32)){
ellipse(windowWidth/2, windowHeight/4*3,100+(50*vol));
} else {
  loading();
  keyReleased();

}


// if(vol >= 0.5){
// loading();
// }

//MicIcon
imageMode(CENTER);
image(imgMic, windowWidth/2, windowHeight/4*3, imgMic.width/5, imgMic.height/5);
image(imgLogo, windowWidth/2, windowHeight/4-50);

//Text + textfont
textFont(ralewayFont);
textSize(15);
textAlign(CENTER);
text('Hold space to ask', windowWidth/2, windowHeight/2+300);

//Call for answer in json
// for (var i = 0; i < tansasBrain.answers.length; i++) {
//   text(random(tansasBrain.answers), windowWidth/2, windowHeight/2);

// createP('h1', tansasBrain.answers[i]);
// if(tansasBrain.answers[i]==1){
//   tansasBrain.answers.splice(i,1)
// }

}

function keyReleased() {
  push();
  noLoop();
if (keyCode === 32) {
  for (var i = 0; i < 1; i++) {
  fill(2,23,142);
  // var repli = replies[i];
  var repli = tansasBrain.answers[Math.floor(Math.random() * tansasBrain.answers.length)];
  textFont(ralewayFont);
  textSize(20)
  textAlign(CENTER)
  text(repli, windowWidth/2, windowHeight/2);
  console.log('Hello World');
  pop();
}
}
}

//THROBBER
function loading(){
push();
  translate(width/2,height/4*3);
  rotate(PI/2);
    noFill();
    stroke(255);
    ellipse(windowWidth/2, windowHeight/4*3,2*r,2*r)
    for(let i=1;i<2 ;i++){
       let ang = radians(-180*cos(radians(k+i)));
       let x = r*cos(ang);
       let y = r*sin(ang);
    			   noStroke();
       fill(2, 23, 142);
       if((k+i)<182)ellipse(x,y,20,20);
    }
    if(k<180){
      k+=1;
    }else{k = 0 };
  pop();
}

function reset(){
  createCanvas(windowWidth, windowHeight);
  mic = new p5.AudioIn();
  mic.start();;
  keyReleased();
}

// function playAnswer(){
//
//
// }
