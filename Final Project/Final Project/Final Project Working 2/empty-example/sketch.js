//audio capture
var mic;

//audio output
var tansaVoice = new p5.Speech;
tansaVoice.setLang('en-UK');
tansaVoice.setVoice('Google UK English Male');

//images
var imgMic;
var imgLogo;

//loading throbber
var k = 99,r = 70;

//text font
var ralewayFont;

//JSON
var tansasBrain
var replies = [];

function preload(){
  tansasBrain = loadJSON('data/TANSASbrain.json')
  imgMic = loadImage('data/micicon.png');
  imgLogo = loadImage('data/tansalogo.png');
  ralewayFont = loadFont('data/Raleway-Light.ttf')
}

function setup(){
createCanvas(windowWidth, windowHeight);
mic = new p5.AudioIn();
mic.start();

}


function draw() {
var vol = mic.getLevel();

background(255);

fill(2,23,142);
noStroke()
ellipse(windowWidth/2, windowHeight/4*3,100);

//interaction

if(keyIsDown(32)){
ellipse(windowWidth/2, windowHeight/4*3,100+(50*vol));
} else {
keyReleased();
}


// if(vol >= 0.5){
// loading();
// }

//MicIcon
imageMode(CENTER);
image(imgMic, windowWidth/2, windowHeight/4*3, imgMic.width/5, imgMic.height/5);
image(imgLogo, windowWidth/2, windowHeight/4-50, imgLogo.width/5, imgLogo.height/5);

//Text + textfont
textFont(ralewayFont);
textSize(20);
textAlign(CENTER);
text('Hold space to ask', windowWidth/2, windowHeight/2+320);
textSize(12);
text('Press R to ask a new question', windowWidth/2, windowHeight/2+350);

//Call for answer in json
// for (var i = 0; i < tansasBrain.answers.length; i++) {
//   text(random(tansasBrain.answers), windowWidth/2, windowHeight/2);

// createP('h1', tansasBrain.answers[i]);
// if(tansasBrain.answers[i]==1){
//   tansasBrain.answers.splice(i,1)
// }

}

function keyReleased() {
if (keyCode === 32) {
loading();
setTimeout(createReplies,5000);
setTimeout(hideThrobber,5000)
setTimeout(noLoop,5000)
}
}

function hideThrobber(){
  //White ellipse in background
  noStroke()
  fill(255)
  ellipse(windowWidth/2, windowHeight/4*3,180)
  //blue ellipse around the mic img, acting as resizer
  fill(2,23,142);
  ellipse(windowWidth/2, windowHeight/4*3,100);
  //new img
  imageMode(CENTER);
  image(imgMic, windowWidth/2, windowHeight/4*3, imgMic.width/5, imgMic.height/5);
}

//THROBBER
function loading(){
push();
  translate(width/2,height/4*3);
  rotate(PI/2);
    noFill();
    stroke(255);
    ellipse(windowWidth/2, windowHeight/4*3,2*r,2*r)
    for(let i=1;i<2 ;i++){
       let ang = radians(-180*cos(radians(k+i))); //radians describes a circular arc that draws from the center of a circle and has the length of the raidus. It is 180/PI degrees
       let x = r*cos(ang);
       let y = r*sin(ang);
    			   noStroke();
       fill(2, 23, 142);
       if((k+i)<182)ellipse(x,y,20,20);
    }
    if(k<180){
      k+=1;
    }else{k = 0 };
  pop();
}

//FETCHING A REPLY//
function createReplies() {
for (var i = 0; i < tansasBrain.answers.length; i++) {
var reply = random(tansasBrain.answers)
  replies.push(reply);
}
for (var i = 0; i < 1; i++) {
var reply = replies[i];

//Text reply
fill(2,23,142);
textFont(ralewayFont);
textSize(20)
textAlign(CENTER)
text(reply, windowWidth/2, windowHeight/2);

//Voice reply
tansaVoice.speak(reply)

}
}

// function refreshPage(){
// setTimeout(function(){
//   location.reload();
// },1);
// }

function keyPressed() {
  if (keyCode === 82) {
  refreshProgram();
  console.log('hi')
}
}

function refreshProgram(){
  location.reload();
}
