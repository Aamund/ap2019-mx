var x = 0;

var l = 0;

function setup() {
  createCanvas(600, 500, WEBGL);
  frameRate(30);
 
  
} 

function draw() {
  background(200,255,200);

  x=millis()/161761;
  
  push();
  fill(255, 128, 0);
  rotateY(degrees(x));
  sphere(40);
  smooth();
  pop();
  push();
  translate(100,100,200);
  fill(153, 204, 255);
  rotateY(degrees(x));
  sphere(12);
  smooth();
  pop();
}
