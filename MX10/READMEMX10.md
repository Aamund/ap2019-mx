**Individual flowchart**

![Screenshot]( Butterfly_game.png )

[Hyperlink to MX6](https://gitlab.com/Aamund/ap2019-mx/tree/master/MX6)

I chose to draw a flowchart of my MX6 which was supposed to be an exploration game programmed with the help of the p5.play library and object-oriented programming. When I wrote my program, I wasn’t happy with the result because the assignment was to create a game and I didn’t believe my program met the criteria. I also thought it was way too simple and therefor boring. But when creating the flowchart for my program I see that it is in fact more advanced than I first thought. I also like breaking the program down into steps and it makes it easier to understand than perhaps reading my code. I see how flowcharts are excellent at explaining algorithms and would be great to use for my MX7, which was a generative program with three simple rules. Flowcharts follow Boolean rules and make it easy for less experienced programmers to understand how a program or algorithm works. The difficulty of drawing a flowchart can be to choose which aspects of a program to incorporate. If your algorithm is advanced you have to know enough about it to choose how to explain what it does and if somethings should be left out. Since my program is simple I did not encounter this challenge.

________________________________________

**Group work:** flowcharts for final project ideas

**TANSA: The artificial AI bot**

![Screenshot]( MX10TANSA.png )

We have the idea of making our own artificial AI bot called TANSA. We want to write the answers of the AI bot as text lines in a JSON file together with sound files of us reading the lines aloud. All in the group will use their voices to record the lines. This is to include both male and female voices and thereby to contribute to the equality debate in digital culture. This is also to break with the manipulation of how consumers perceive information given from different genders. Male voices have a tendency to carry more authority. Female voice tends to be more persuasive. We are not skilled enough to make a real interaction with AI. So, one of the main challenges of this program is that we want to imitate a mic input interaction with our TANSA bot giving the illusion of the bot answering your question. To do that, we might have to make a conditional statement saying that the button has to be pressed for at least 2 seconds before the bot fetches an answer. While pressing the mic button a throbber will indicate that the button is pressed. We are going to use the boolean statement of mousePressed meaning that the mic button has to be held down and then released before the JSON data is called. Another option for constraining the JSON data access is to let the mic input decide it. So, if the program captures a specific volume level the JSON data is accessed and fetched. We have called our AI bot ‘TANSA’ which is an acronym for There Are No Stupid Answers. We want to make a critical statement with this program, by somewhat ridiculing the way we tend to lay our trust in machines. So, we basically want people to reconsider their use of machines and make them engage with machines with a more critical sense, not taking everything for gospel truth.

**The sound of Automation**

![Screenshot]( MX10automation.png )

When is art really art? Does it have to be created by a human or can a machine generate art? We want to test this with a program that generates a piece of digital art combined with music. We as creators of the program will set some rules for the program to execute and thereby give the machine the possibility to create a piece of art on its own. This means that there are still humans behind it, making us the overall authors.
We want to create a program that generates art inspired by Mathias’ mini_ex7 where he created a generative art piece consisting of ellipses and frames of other shapes appearing when the ellipses would reach specific points. We have used similar design elements by making ellipses appear from the middle of the canvas, moving randomly and creating a randomly generated pattern. But we have redesigned the behavior of it crossing the borders. When it reaches either the bottom, top, right or left border a sound of a chord will play and a geometric shape will appear, continuously growing in size. One of the challenges with this idea is that we are unsure of how to implement the sound output when the ellipses reach the borders
________________________________________

**Individual:** How is this flowchart different from the one that you had in #3 (in terms of the role of a flowchart)?

When I drew the flowchart for my MX6 I had already written the code for the program and made it work. I knew what my program did and how it did it, which made it really easy to write down what rules it follows. I simply had to look at what expressions that had to do with the actions the user can take and translate them to a flowchart. With the flowcharts for MX10 we had not yet created the programs. The code has not yet been written so they serve as a plan of action for the program. Like the three rules we had to write down before beginning to code during our MX7, the flowcharts are meant to be goals for our final program.

**Individual:** If you have to bring the concept of algorithms from flowcharts to a wider cultural context, how would you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)
