![screenshot]( Screen_Shot_2019-03-04_at_10.09.41.png)

[link](https://cdn.staticaly.com/gl/Aamund/ap2019-mx/raw/master/MX4/sketch.js)


So this for this mini exercise I had a more heavy focus on the hidden and satirical message of my program. I wanted to show how by simply clicking the like button on facebook you contribute to a global capitalist economy that favors the 1%. The cheer sound effect also contributes to the humorous side of the program and exaggerates the message.

The data capture element is simply a button, but everybody is able to see how many times you have clicked to button by counting the amount of coins on the screen. Therefore there is an economic transparancy in my program that which you cannot find anywhere else. You can literally see the consequence of your like.

My code is very simple but you will see bits that may confuse you. I have too many variables and a few uneccesary lines of code because I tried to have the coins move down the screen when they are generated. Instead I got the effect of having the coins generate randomly on the x axis and 1 pixel below the previous generated coin on the y axis. I believe this is because my code isn't in the draw function, but the entire program ceases to work when I move the code up. Another hiccup that I have not solved is the image of the coin itself. I clipped the coin out and saved it as a .png file but it still has the white square border. A small thing but it still annoys the eye. 

PS. Maybe you need to open the link in firefox or another browser than chrome if it doesn't work!