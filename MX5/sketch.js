var x = 0;

var l = 0;

var slider1;

var slider2;

var slider3;

var amt, startColor, newColor;

function setup() {
  createCanvas(600, 500, WEBGL);
  frameRate(30);
  slider1 = createSlider(0, 255, 100);
  slider1.position(10, 10);
  slider1.style('width', '80px');

  slider2 = createSlider(0, 255, 100);
  slider2.position(10, 50);
  slider2.style('width', '80px');

  slider3 = createSlider(0, 255, 100);
  slider3.position(10, 90);
  slider3.style('width', '80px');

    startColor = color(255,255,255);
    newColor = color(random(255),random(255),random(255));
    amt = 0;

    background(startColor);

}

function draw() {
  background(lerpColor(startColor, newColor, amt));
    amt += 0.01;
    if(amt >= 1){
      amt = 0.0;
      startColor = newColor;
      newColor = color(random(255),random(255),random(255));
    }
  var val1 = slider1.value();
  var val2 = slider2.value();
  var val3 = slider3.value();
  x=millis()/160000;

  push();
  fill(val1, 128, 0);
  rotateY(degrees(x));
  sphere(40);
  smooth();
  pop();
  push();
  translate(100,100,200);
  fill(val2, 204, 255);
  rotateY(degrees(x));
  sphere(12);
  smooth();
  pop();
  push();
  translate(-75,40,100);
  fill(val3, 50, 190);
  rotateY(degrees(x));
  sphere(25);
  smooth();
  pop();
}
