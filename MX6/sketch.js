var ghost;
var bg;
var flower;
var flowers=[];

var SCENE_W = 5000;
var SCENE_H = 3000;

function preload() {

flower=loadImage('flower1.png');

}

function setup() {
  createCanvas(800, 400);
  frameRate(30);

  // for (var i = 0; i < 400; i++){
  //   flowers[i] = {
  //
  //     x: random(0,width),
  //     y: random(0,height),
  //     display:function(){
  //       fill(255);
  //       ellipse(random(-width, SCENE_W+width), random(-height, SCENE_H+height),50,50);
  //           }
  //   }
  // }



  ghost = createSprite(400, 200, 50, 100);

  var myAnimation = ghost.addAnimation('floating', 'butterfly-1.png', 'butterfly-2.png', 'butterfly-3.png');

  ghost.addAnimation('moving', 'butterfly-1.png', 'butterfly-2.png', 'butterfly-3.png');

  bg = new Group();


  for(var i=0; i<500; i++)
  {
    var rock = createSprite(random(-width, SCENE_W+width), random(-height, SCENE_H+height));
    rock.addAnimation('normal','palm.png');
    bg.add(rock);
  }


}

function draw() {
  background(116, 243, 150);

//   for (let i = 0; i < flowers.length; i++){
// 	flowers[i].display();
// }

//}
  ghost.velocity.x = (camera.mouseX-ghost.position.x)/20;
  ghost.velocity.y = (camera.mouseY-ghost.position.y)/20;


  if(mouseIsPressed)
    camera.zoom = 0.5;
  else
    camera.zoom = 1;

  camera.position.x = ghost.position.x;
  camera.position.y = ghost.position.y;

  if(ghost.position.x < 0)
    ghost.position.x = 0;
  if(ghost.position.y < 0)
    ghost.position.y = 0;
  if(ghost.position.x > SCENE_W)
    ghost.position.x = SCENE_W;
  if(ghost.position.y > SCENE_H)
    ghost.position.y = SCENE_H;


  drawSprites(bg);

  noStroke();
  fill(0, 0, 0, 20);
  ellipse(ghost.position.x-5, ghost.position.y+40, 20, 10);
  drawSprite(ghost);


  camera.off();

}

// class Flower {
// constructor(xpos,ypos) {
//
//   this.xpos=xpos;
//   this.ypos=ypos;
//
// }
//
// show(){
//   image(this.flower,this.xpos,this.ypos);
// }
//
//
//
//
//
// }
