![screenshot](Screen_Shot_2019-03-25_at_17.25.38.png)

[link](https://cdn.staticaly.com/gl/Aamund/ap2019-mx/raw/master/MX7/index.html)

Creating a program with only 3 simple rules for automation is harder than it sounds. Having three rules decide the outcome of the entire program gives them so much power that they each need to be well thought and executed. There can be no mistakes and the rules have to cooperate to create a program sophisticated enough to be pleasing and advanced at the same time.  

My rules are:
1.	For each frame, one coloured dot with a diameter of five pixels and five white dots are drawn with a diameter of one pixel.
2.	Once a dot has been drawn, it shifts between two sizes
3.	All drawn dots move randomly around the same area they have been drawn

My rules create a program that looks like a starry sky with larger coloured planets. All the stars “twinkle” by changing size between 1 and 2 pixels. And every star and planet move around between each other, but only in the area they have been born in. This resembles a timelapse of how the real universe is observed. Stars and planets moving around between each other, shining their light on us.

My program automatically creates a universe unlike any other by randomly generating the entities of which the universe contains. Planets and stars are born automatically in sequence, but at a random location and that location will be their home for the rest of their lives. They can move around their home but they can never leave. Stuck in orbit they all coexist. 
