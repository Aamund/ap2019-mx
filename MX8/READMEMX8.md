![screenshot](Screen_Shot_2019-03-29_at_18.22.17.png)

[link](https://cdn.staticaly.com/gl/Aamund/ap2019-mx/raw/master/MX8/empty-example/index.html)

For this week’s mini exercise, I worked together with Mathias Rahbek. We agreed upon wanting to create a story that tells itself. We would create a template for the story and the program would fill in the blanks we created with random words that we stored in a JSON file. That way, the structure of the story was always kept intact, but the story itself would never be the same. We refined the story by choosing the form of a fairy tale, since the fairy tale like a haiku poem or a sonnet has a very strict form. It always starts with ‘Once upon a time’ and ends with ‘And the lived happily ever after.’ This is to tie the story together and give the reader a sense of progression. 

We wrote a prototype fairy tale and chose the words we wanted to be random. Then we looked at what type of word each individual word was and created a “type” in our JSON file. We recognized nouns, adjectives, verbs and a few subclasses and began finding words that belong to these types. Each type of word in our JSON file has the same amount of letters +- 1 for formatting purposes. We then made a for loop for each type of word in our sketch and placed a text function in the “blank” spaces in our template. The font, size and color is the same for the entire fairy tale so that it seems like the story is entirely written by the program.

We got the idea from the Love Letters written by the M.U.C. created by Christopher Strachey and chose to create our own variant of his program. 

Many of the challenges we faced in creating our code can be traced back to how language itself is built. We considered creating a type of word for each individual blank space because the words we use can have drastically different meaning depending on the context. Geoff Cox and Alex McLean says, “…all languages consist of closed systems of discrete semiotic elements, and that meaning is organized out of differences between elements that are meaningless in themselves.”
Because our program doesn’t differentiate between what, let’s say adjectives, are used, there are no difference between elements and a lot of meaning is therefore lost.
